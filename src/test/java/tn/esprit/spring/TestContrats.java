package tn.esprit.spring;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.services.IContratService;
import tn.esprit.spring.services.IEmployeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestContrats {

	@Autowired
	IEmployeService emp;
	@Autowired
	IContratService con;

	@Test
	public void testAuthenticate() {
		Employe e = new Employe("f", "f", "marwenguesmi@esprit.tn", "marwen15852", false, Role.ADMINISTRATEUR);
		Employe Authentification = emp.authenticate("f", "f");

		assertEquals("f", Authentification.getEmail());
	}

	@Test
	public void testgetAllContrats() {
		List<Contrat> contrat = con.getAllContrats();
		assertEquals(1,contrat.size());
	}
}
